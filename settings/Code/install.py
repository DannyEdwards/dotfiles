#!/usr/bin/env python3
import os
import getpass

# Get current working directory
cwd = os.getcwd() + "/settings/Code"
filename = "settings.json"

# platform = "Mac" if os.name == "posix" else "Linux" # @todo Create a Linux install
# print("Installing settings for VSCode on %s" % platform)

file = "%s/%s" % (cwd, filename)
target = "/Users/%s/Library/Application Support/Code/User/%s" % (getpass.getuser(), filename)

if os.path.islink(target):
    print("Link already created for %s" % target)
else:
    print("%s -> %s" % (file, target))
    os.symlink(file, target)
