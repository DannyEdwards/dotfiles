# .dotfiles

Dan's dotfiles.

## Installation

First, install [oh my zsh](https://ohmyz.sh/) and the [Fira Code iScript](https://github.com/kencrocken/FiraCodeiScript) font.

Now clone this repository into your home folder then `cd` into the project.

```bash
cd ~
git clone git@gitlab.com:DannyEdwards/dotfiles.git
cd dotfiles
```

Run the `install.py` shell.

```bash
./install.py
```

You may have to adjust the permissions on the installation script so that it is executable (`chmod u+x install.py`).

## Uninstalling

Run the same script with the `-u` flag.

```bash
./install.py -u
```

## System Settings

System settings are also stored and symlinked into the machine's file structure.

Each setting directory needs it's own `install.py` and relevant files.

## Command Line Applications

Custom scripts/application binaries can be put into `bin/` and automatically picked up in the `PATH`. Just be sure to make the file executable.

```bash
chmod u+x bin/*
```
