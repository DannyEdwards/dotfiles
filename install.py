#!/usr/bin/env python3
import sys
import os

# Get some useful paths
cwd = os.getcwd()
home = os.path.expanduser("~")
vimconf = "%s/.vim" % home

# Get the first argument
try:
    arg = sys.argv[1]
except IndexError:
    arg = False

# Let the user know what we are doing
print("%s dotfiles\n" % ("Uninstalling" if arg == "-u" else "Installing"))

print("Removing existing symlinks...\n")

# Get rid of the existing dotfiles
for dir, subdirs, files in os.walk("./runcoms/linked"):
    for file in files:
        target = "%s/.%s" % (home, file.split(".")[0])
        if os.path.islink(target):
            print("Removing %s" % target)
            os.unlink(target)

# Remove vim symlink
if os.path.islink(vimconf):
    print("Removing %s" % vimconf)
    os.unlink(vimconf)

# Re/Install them
if arg != "-u":
    print("\nSymlinking dotfiles...\n")
    for dir, subdirs, files in os.walk("./runcoms/linked"):
        for file in files:
            target = "%s/.%s" % (home, file.split(".")[0])
            print("%s/runcoms/linked/%s -> %s" % (cwd, file, target))
            os.symlink("%s/runcoms/linked/%s" % (cwd, file), target)
    # Relink vim
    print("%s/runcoms/vim -> %s" % (cwd, vimconf))
    os.symlink("%s/runcoms/vim" % cwd, vimconf)

# System settings
print("\nLinking system settings files...\n")

for dir, subdirs, files in os.walk("./settings"):
    for subdir in subdirs:
        install = "./settings/%s/install.py" % subdir
        if os.path.isfile(install):
            os.system(install)
        else:
            print("There is no `install.py` inside the `%s` directory" % subdir)

print("\nDone!")
