" Settings
""""""""""
syntax enable
set number " Line numbers
set hidden " Do not prompt for save on buffer switch
set hlsearch " Search highlight
set mouse=a " Mouse support
set nocompatible " Disables VI commands
set laststatus=2
set statusline=%#Highlight#\ %{DisplayMode()}\ %*\ %F%m%r%h\ \ Line:\ %l\ \ Column:\ %c
set noshowmode
highlight Highlight ctermfg=white ctermbg=magenta

" Keys
""""""
vnoremap <C-c> "*y

" Filetypes
"""""""""""
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown

" Functions
"""""""""""
function! DisplayMode() abort
  let l:mode = mode()
  if l:mode =~ '[vV]'
    return 'VISUAL'
  elseif l:mode ==# 'i'
    return 'INSERT'
  elseif l:mode ==# 'R'
    return 'REPLACE'
  elseif l:mode ==# 'c'
    return 'COMMAND'
  elseif l:mode ==# 't'
    return 'TERMINAL'
  elseif l:mode ==# 'n'
    return 'NORMAL'
  else
    return toupper(l:mode[0])
  endif
endfunction
