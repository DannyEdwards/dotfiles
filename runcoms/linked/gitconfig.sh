[alias]
	s = status
	b = branch
	c = commit
	c-a = commit --amend
	c-wip = commit -m 'feat: wip'
	co = checkout
	co-- = checkout --
	push-u = push -u origin HEAD
	push-fwl = push --force-with-lease
	undopush = push -f origin HEAD^:master
	irebase = "!f() { git rebase -i HEAD~$1; }; f"
[branch]
    sort = -committerdate
[color]
	ui = true
[column]
    ui = auto
[commit]
	template = ~/.dotfiles/runcoms/git-commit-template
[core]
	excludesfile = ~/.dotfiles/runcoms/gitignore_global
[diff]
    algorithm = histogram
    colorMoved = plain
    mnemonicPrefix = true
    renames = true
[fetch]
    prune = true
    pruneTags = true
    all = true
[filter "media"]
	clean = git-media-clean %f
	smudge = git-media-smudge %f
[filter "lfs"]
	clean = git-lfs clean -- %f
	process = git-lfs filter-process
	required = true
	smudge = git-lfs smudge -- %f
[merge]
	tool = opendiff
[pager]
    branch = false
    tag = false
[pull]
	rebase = false
[push]
    followTags = true
[tag]
    sort = version:refname
[user]
	email = igneosaur@gmail.com
	name = Dan Edwards
