echo "Jurassic Park, System Security Interface"
echo "Version 4.0.5, Alpha E"
echo "Ready..."

# Paths
# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH=$HOME/.dotfiles/bin:$PATH # Personal binaries
export PATH="/Applications/Visual Studio Code.app/Contents/Resources/app/bin":$PATH # VSCode
export PATH=$HOME/.composer/vendor/bin:$PATH # PHP packages
export PATH=$HOME/Library/Android/sdk/tools:$HOME/Library/Android/sdk/platform-tools:$PATH # Android SDK
export PATH=$HOME/Library/Python/2.7/bin:$PATH # Python packages
export PATH=$HOME/Library/Apache/Maven/bin:$PATH # Maven
export PATH=$HOME/.cargo/bin:$PATH # Cargo (Rust) packages
export PATH=$HOME/.deno/bin:$PATH # Deno
export PATH=$HOME/Library/pnpm:$PATH # PNPM
export PATH=$HOME/.bun/bin:$PATH # Bun
export PATH=$HOME/.lmstudio/bin:$PATH # LM Studio CLI

# Search paths (completions)
if [[ ":$FPATH:" != *":$HOME/.zsh/completions:"* ]]; then
    export FPATH="$HOME/.zsh/completions:$FPATH"
fi
[ -s "/Users/dan/.bun/_bun" ] && source "/Users/dan/.bun/_bun"

# Environment variables
export ZSH="$HOME/.oh-my-zsh"
export DEFAULT_USER=$(whoami) # @todo
export EDITOR=vim
export VISUAL=vim
export MANPAGER="less -X" # Don’t clear after quitting a manual page
export HISTIGNORE="ls:ls *:cd:cd -:pwd;exit:date:* --help" # Hide some commands in history
export CLOUD_DRIVE=$HOME/pCloud\ Drive
export HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=true
export HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND=fg=bold,underline
export HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND=fg=red,bold

case $(uname) in
   Linux)
      export MACHINE=linux
      ;;
   Darwin)
      export MACHINE=mac
      ;;
   "FreeBSD NetBSD DragonFly")
      export MACHINE=bsd
      ;;
   *)
     export MACHINE=unknown
     ;;
esac

if [[ $MACHINE = "linux" ]] then
  export FILEMANAGER=nemo
else
  export FILEMANAGER=open
fi

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git zsh-autosuggestions zsh-history-substring-search)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_GB.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
alias g=git
alias v=vim
alias open=$FILEMANAGER
alias hosts="sudo $EDITOR /etc/hosts"
alias ip="dig +short myip.opendns.com @resolver1.opendns.com"
alias iplocal="ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'"
alias localip="iplocal"
alias whois="whois -h whois-servers.net"
alias fs="stat -f \"%z bytes\"" # File size
alias cleanup="find . -name '*.DS_Store' -type f -ls -delete" # Recursively delete `.DS_Store` files
alias gti="print -P '%F{red}🚗 💨 Vroooooom!%f'; git"
alias www="cd /var/www"
alias phpunit="./vendor/bin/phpunit"
alias rmnodemodules="find . -name 'node_modules' -type d -prune -print -exec rm -rf '{}' \;"
alias npmupdate="npx npm-check-updates -i"

# Key bindings
bindkey '^[OA' history-substring-search-up
bindkey '^[OB' history-substring-search-down

# Custom configuration
if [ -f $HOME/.dotfiles/runcoms/custom/zshrc.sh ]; then
   source $HOME/.dotfiles/runcoms/custom/zshrc.sh
fi
